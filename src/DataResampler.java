import java.util.ArrayList;
import java.util.List;

/**
 * User: guoxc
 * Date: 14-3-24
 * Time: 15:08
 */
public class DataResampler {

    public int frequency;

    public int interval;

    public int start_point = 10;
    public int end_offset = 0;

    public DataResampler(int frequency){
        this.frequency = frequency;
        this.interval = 1000000000 / frequency;
    }

    public ArrayList<DataPack> getResampledData(List<DataPack> source){
        ArrayList<DataPack> result = new ArrayList<DataPack>();
        int source_size = source.size();
        long start_timestamp = source.get(start_point).timestamp;
        long end_timestamp = source.get(source_size - end_offset - 1).timestamp;
        int j = start_point;
        for(long i = start_timestamp; i < end_timestamp; i += interval){
            while(source.get(j).timestamp < i){
                j += 1;
            }
            DataPack newData = (DataPack)source.get(j).clone();
            newData.timestamp = i;
            result.add(newData);
        }
        return result;
    }
}
