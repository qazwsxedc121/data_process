import java.io.DataInputStream;

/**
 * User: guoxc
 * Date: 14-3-17
 * Time: 15:18
 */
public class DataPack implements Cloneable{
    public float x;
    public float y;
    public float z;
    public long timestamp;
    public DataPack(DataInputStream dis) throws Exception{
        this.x = dis.readFloat();
        this.y = dis.readFloat();
        this.z = dis.readFloat();
        this.timestamp = dis.readLong();
    }

    public DataPack() {

    }

    public Object clone(){
        DataPack newData = new DataPack();
        newData.x = this.x;
        newData.y = this.y;
        newData.z = this.z;
        newData.timestamp = this.timestamp;
        return newData;
    }

    public double getVectorLength(){
        return Math.sqrt((x * x) + (y * y) + (z * z));
    }

    public String toString(){
        return this.x + " " + this.y + " " + this.z + "\n";
    }
}
