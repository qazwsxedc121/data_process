import java.io.DataInputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

public class DataReader {
    public ArrayList<DataPack> getData(String src){
        ArrayList<DataPack> result = new ArrayList<DataPack>();
        try{
            DataInputStream fio = new DataInputStream(new FileInputStream(src));
            while(fio.available() > 0){
                result.add(new DataPack(fio));
            }
            fio.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

}
