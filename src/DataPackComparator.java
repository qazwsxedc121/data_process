import java.util.Comparator;

/**
 * User: guoxc
 * Date: 14-3-24
 * Time: 15:00
 */
public class DataPackComparator implements Comparator<DataPack> {

    public static final double threshold = 0.000000001;

    @Override
    public int compare(DataPack dataPack, DataPack dataPack2) {
        if(dataPack.getVectorLength() - dataPack2.getVectorLength() > threshold){
            return 1;
        }else if(dataPack2.getVectorLength() - dataPack.getVectorLength() < threshold){
            return -1;
        }else{
            return 0;
        }
    }
}
