import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * User: guoxc
 * Date: 14-3-13
 * Time: 15:14
 */
public class MedianFilter extends Filter {
    private int window = 3;
    private int lwindow = 1;
    public MedianFilter(int window){
        if(window % 2 == 0){
            throw new Error("window not odd");
        }
        this.window = window;
        this.lwindow = (window - 1) / 2;
    }

    @Override
    public List<DataPack> filter(List<DataPack> tobefiltered) {
        int i = 0;
        int list_size = tobefiltered.size();
        List<DataPack> result = new ArrayList<DataPack>();
        for(i = 0; i < (list_size - 1); i += 1){
            List<DataPack> pool = new ArrayList<DataPack>();
            for(int k = i - lwindow; k <= i + lwindow; k += 1){
                int j = k < 0 ? 0 : k;
                j = j >= (list_size - 1) ? list_size - 1 : j;
                pool.add(tobefiltered.get(j));
            }
            result.add(this.getMedian(pool));
        }
        return result;
    }

    private DataPack getMedian(List<DataPack> pool){
        Collections.sort(pool, new DataPackComparator());
        return pool.get(this.lwindow);
    }
}
