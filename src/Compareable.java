/**
 * User: guoxc
 * Date: 14-3-13
 * Time: 15:04
 */
public interface Compareable {
    public boolean lt(Compareable b);
    public Double toDouble();
}
