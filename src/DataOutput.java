import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

/**
 * User: guoxc
 * Date: 14-4-1
 * Time: 15:16
 */
public class DataOutput {
    public static void toText(List<DataPack> data, String newfilename) throws  Exception{
        File f = new File(newfilename);
        f.getParentFile().mkdirs();
        BufferedWriter fw = new BufferedWriter(new FileWriter(f));
        for(int i = 0; i < data.size(); i += 1){
            fw.write(Float.toString(data.get(i).x));
            fw.write(" ");
            fw.write(Float.toString(data.get(i).y));
            fw.write(" ");
            fw.write(Float.toString(data.get(i).z));
            fw.newLine();
        }
        fw.close();
    }
}
