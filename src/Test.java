import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * User: guoxc
 * Date: 14-3-13
 * Time: 15:37
 */
public class Test {
    public static void main(String[] args) throws Exception{
        test3();
    }
    public static void test1(){
        String data_str = "1,2,10,4.7,5,2,1,5";
        String[] data_str_array = data_str.split(",");
        List<DataPack> tobefiltered = new ArrayList<DataPack>();
        for(int i = 0; i < data_str_array.length; i += 1){

        }
        MedianFilter filter = new MedianFilter(3);
        List<DataPack> filted = filter.filter(tobefiltered);
        System.out.print(filted.toString());
    }
    public static void test2(){
        ArrayList<DataPack> s = new DataReader().getData("data/gyro_2_4s.dat");
        ArrayList<DataPack> s1 = new DataResampler(50).getResampledData(s);
        System.out.println(s1.get(0).timestamp);
        System.out.println(s1.size());
        System.out.println(s1.get(s1.size() - 1).timestamp - s1.get(0).timestamp);
        MedianFilter filter = new MedianFilter(3);
        List<DataPack> filted = filter.filter(s1);
        System.out.print(filted.toString());

    }
    public static void test3() throws Exception{
        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        FileInputStream in = new FileInputStream("config.xml");
        Document document=db.parse(in);
        Element root = document.getDocumentElement();
        Element input = (Element)root.getElementsByTagName("input").item(0);

        //parse single data file path
        ArrayList<String> paths = new ArrayList<String>();
        NodeList files = input.getElementsByTagName("file");
        for(int i = 0; i < files.getLength(); i += 1){
            paths.add(files.item(i).getAttributes().getNamedItem("path").getNodeValue());
        }

        //parse data directory path
        ArrayList<String> dirpaths = new ArrayList<String>();
        NodeList dirs = input.getElementsByTagName("dir");
        for(int i = 0; i < dirs.getLength(); i += 1){
            dirpaths.add(dirs.item(i).getAttributes().getNamedItem("path").getNodeValue());
        }

        //pipe setting
        Element pipe = (Element) root.getElementsByTagName("pipe").item(0);
        int freq = Integer.parseInt(pipe.getElementsByTagName("resampling").item(0).getAttributes().getNamedItem("frequency").getNodeValue());
        DataResampler resampler = new DataResampler(freq);
        DataOutput output = new DataOutput();
        Element filter = (Element) pipe.getElementsByTagName("filter").item(0);
        String filter_type = filter.getAttribute("type");
        Filter filter1 ;
        if(filter_type.equals("median")){
            int window = Integer.parseInt(filter.getAttribute("window"));
            filter1 = new MedianFilter(window);
        }else{
            filter1 = new MedianFilter(3);
        }

        //process single files
        for(int i = 0; i < paths.size(); i += 1){
            ArrayList<DataPack> s = new DataReader().getData(paths.get(i));
            ArrayList<DataPack> s1 = resampler.getResampledData(s);
            List<DataPack> filted = filter1.filter(s1);
            output.toText(filted, "output/"+paths.get(i).substring(0,paths.get(i).length()-3) + "txt");
        }

        //process dir data files
        for(int i = 0; i < dirpaths.size(); i += 1){
            String dirpath = dirpaths.get(i);
            File dir = new File(dirpath);
            File[] dir_file_paths = dir.listFiles();
            for(int j = 0; j < dir_file_paths.length; j += 1){
                String temp_path = dir_file_paths[j].getPath();
                ArrayList<DataPack> s = new DataReader().getData(temp_path);
                ArrayList<DataPack> s1 = resampler.getResampledData(s);
                List<DataPack> filterd = filter1.filter(s1);
                output.toText(filterd, "output/"+temp_path.substring(0,temp_path.length() - 3) + "txt");
            }
        }

        in.close();

    }
}
